import $ from 'jquery'
import Popup from '../modules/module.popup'
import Form from '../modules/module.validate'
import Inputmask from 'inputmask'

window.app = window.app || {};

window.app.popup = new Popup();


$(function () {

	const form = new Form();

	let $b = $('body');

	$('input[data-mask]').each(function () {
		let $t = $(this),
			inputmask = new Inputmask({
			mask: $t.attr('data-mask'),		//'+7 (999) 999-99-99',
			showMaskOnHover: false,
			onincomplete: function() {
				this.value = '';
				$t.closest('.form__field').removeClass('f-filled');
			}
		});
		inputmask.mask($t[0]);
	});

	$b
		.on('click', '[data-scroll]', function (e) {
			let $t = $(this),
				hash = $t.attr('data-scroll');

			if ( $(hash).length > 0){
				e.preventDefault();
				$('html,body').animate({scrollTop:$(hash).offset().top + 1}, 500);
			}
		})

		.on('blur', '.form__field input, .form__field textarea', function (e) {
			let $input = $(this),
				$field = $input.closest('.form__field');
			$field.removeClass('f-focused');

			if ( !$.trim($input.val()) ){
				$field.removeClass('f-filled');
			} else {
				$field.addClass('f-filled');
			}
		})

		.on('submit', '.form', function (e) {
			let $form = $(this),
				$item = $form.find('input'),
				wrong = false
			;

			$item.each(function () {
				let input = $(this), rule = $(this).attr('data-require');
				$(input)
					.closest('.form__field')
					.removeClass('f-error f-message')
					.find('.form__message')
					.html('')
				;

				if ( rule ){
					form.validate(input[0], rule, err =>{
						if (err.errors.length) {
							wrong = true;
							$(input)
								.closest('.form__field')
								.addClass('f-error f-message')
								.find('.form__message')
								.html(err.errors[0])
							;
						}
					})
				}
			});

			if ( wrong ){
				e.preventDefault();
			}
		})



});


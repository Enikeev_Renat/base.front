'use strict';

import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import fs from 'fs';
import yargs from 'yargs';
import browserSync from 'browser-sync';
let bs = browserSync.create();
let argv = yargs.argv;


let $ = gulpLoadPlugins({
	DEBUG: false,
	pattern: ['*'],
	replaceString: /^(vinyl|gulp)(-|\.)/,
	camelize: true
});

import PATH from './gulp-tasks/path.json';


// browserSync
gulp.task('bs:init', ()=>{
	bs.init({
		server: {
			baseDir: PATH.DIR.PUBLIC
		},
		notify: true
	});
});

let bsReload = ()=>{
	if (argv.bs) bs.reload();
};


function jsCompile(file, callback) {
	//console.log(`${PATH.JS.SOURCE}script/${file}`);
	return $.browserify(`${PATH.JS_BUNDLE.SOURCE}${file}`, { debug: true }).transform($.babelify).bundle()
		.on('error', function(error){
			$.util.log(error.message);
			this.emit('end')
		})
		.pipe($.sourceStream('.'))
		.pipe($.buffer())
		.pipe($.rename({
			basename: file.replace(/(\.js)$/, ''),
			suffix: '.script',
			extname: '.js'
		}))
		.pipe($.sourcemaps.init({ loadMaps: true }))
		.pipe($.sourcemaps.write(PATH.JS_BUNDLE.MAPS))
		.pipe(gulp.dest(PATH.JS_BUNDLE.PUBLIC))
		.on('end', bsReload);
}

// js babel js
gulp.task('js-bundle', ()=>{
	fs.readdir(`${PATH.JS_BUNDLE.SOURCE}`, function(err, files) {
		if (err) return console.log(err);
		files.forEach(function(file) {
			jsCompile(file);
		});
	});
});
gulp.task('js-bundle:watch', ()=>{
	return $.watch(`${PATH.JS_BUNDLE.SOURCE}*.js`, (f)=>{
		let regex = /([\w\d-_]+.js)$/g,
			file = f.path.match(regex)[0];
		jsCompile(file, bsReload);
	});
});






// clean public

gulp.task('clean', () => {
	return gulp.src(PATH.DIR.PUBLIC, {read: false})
		.pipe($.clean());
});





// pug -> html
gulp.task('html', getTask('html'));
gulp.task('html:watching', ['html'], bsReload);
gulp.task('html:watch', ()=>$.watch(`${PATH.PAGES.SOURCE}**/*.pug`, ()=>gulp.start('html:watching')));


// js vendor -> vendor.js
gulp.task('js-vendor', getTask('js-vendor'));
gulp.task('js-vendor:watching', ['js-vendor'], bsReload);
gulp.task('js-vendor:watch', ()=>$.watch(`${PATH.JS.SOURCE}vendor/**/*.js`, ()=>gulp.start('js-vendor:watching')));

// js root -> js
gulp.task('js-root', getTask('js-root'));
gulp.task('js-root:watching', ['js-root'], bsReload);
gulp.task('js-root:watch', ()=>$.watch(`${PATH.JS.SOURCE}*.js`, ()=>gulp.start('js-root:watching')));


// sass -> css
gulp.task('style', getTask('style'));
gulp.task('style:watching', ['style'], bsReload);
gulp.task('style:watch', ()=>$.watch(`${PATH.CSS.SOURCE}**/*.scss`, ()=>gulp.start('style:watching')));


// img -> img
gulp.task('image', getTask('image'));
gulp.task('image:watching', ['image'], bsReload);
gulp.task('image:watch', ()=>$.watch([`${PATH.IMG.SOURCE}**/*`, `!${PATH.IMG.SOURCE}sprites`, `!${PATH.IMG.SOURCE}sprites/**/*`], ()=>gulp.start('image:watching')));



// sprites -> img
gulp.task('getSprites', getTask('sprites'));

// files transfer

// data -> data
gulp.task('data', getTask('data'));

// fonts -> fonts
gulp.task('fonts', getTask('fonts'));

// robots.txt -> robots.txt
gulp.task('robots', getTask('robots'));




// pages map
gulp.task('pagesList', getTask('pages-list'));

// favicon generator
gulp.task('favicon', getTask('favicon'));





gulp.task('build', ['js-bundle', 'js-vendor', 'js-root', 'image', 'html', 'style', 'robots', 'data', 'fonts'], ()=>{
	if ( argv.bs ) gulp.start('bs:init');
});

gulp.task('watch', ['js-bundle:watch', 'js-vendor:watch', 'js-root:watch', 'image:watch', 'html:watch', 'style:watch']);

gulp.task('default', ['build', 'watch']);



function getTask(task) {
	let args = {gulp, $, PATH, fs, argv};
	return require('./gulp-tasks/' + task)(args);
}



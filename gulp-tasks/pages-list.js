module.exports = function (args) {
	let gulp = args.gulp, $ = args.$, PATH = args.PATH, fs = args.fs;
	return ()=> fs.readdir(PATH.PAGES.SOURCE, function(err, files) {
		if (err) return console.log(err);

		let html, regex = /([\w\d-_]+.pug)$/g;
		html = `<!DOCTYPE html><html><head><meta charset="UTF-8"/><title>Project pages list</title></head><body><h1>Pages:</h1>`;

		files.forEach(function(file) {
			if (file.match(regex)){
				//console.log(file.match(regex));
				let fileName = file.match(regex)[0].replace(/(.pug)$/g, '');
				html += `<p><a href="/${fileName}.html">${fileName}</a></p>`;
			}
		});

		html += `</body></html>`;

		fs.writeFile(`${PATH.DIR.PUBLIC}pages.html`, html, function (err) {
			if (err) return console.log(err);
			console.log('Pages map generated');
		});
	});
};

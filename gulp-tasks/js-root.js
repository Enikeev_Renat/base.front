module.exports = function (args) {
	let gulp = args.gulp, $ = args.$, PATH = args.PATH;
	return ()=> gulp.src(`${PATH.JS.SOURCE}*.js`)
		.on('error', function(error){
			$.util.log(error.message);
			this.emit('end');
		})
		.pipe(gulp.dest(PATH.JS.PUBLIC));
};

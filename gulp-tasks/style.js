module.exports = function (args) {
	let gulp = args.gulp, $ = args.$, PATH = args.PATH;
	return ()=> gulp.src(`${PATH.CSS.SOURCE}*.scss`)
		.pipe($.sourcemaps.init())
		.pipe($.sass({
			outputStyle: 'compact'
		}).on('error', function(error){
			$.util.log(error.message);
			this.emit('end');
		}))
		.pipe($.sourcemaps.write(PATH.CSS.MAPS))
		.pipe(gulp.dest(PATH.CSS.PUBLIC))
		.on('error', function(error){
			$.util.log(error.message);
			this.emit('end');
		});
};
